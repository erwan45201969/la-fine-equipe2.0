from box import *

def test_box_create():
    b = Box()

def test_box_contains():
    b= Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "truc2" in b
    assert "tachanka" not in b

def test_remove_content():
    b= Box()
    b.add("ash")
    b.add("nomad")
    b.remove("ash")
    assert "nomad" in b
    assert "ash" not in b

def test_ouverte():
    b= Box()
    assert not b.is_open()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_action_look():
    b= Box()
    b.add("truc1")
    b.add("truc2")
    assert b.action_look() == "la boite est fermee"
    b.open()
    assert b.action_look() == "la boite contient:truc1,truc2"

def test_creerChose():
    c=Chose(3,"chose")
    assert c.getname()=="chose"
    assert c.has_name("chose")
    assert c.getvolume() == 3

def test_box_capacity():
    b=Box()
    b.set_capacity(100)
    assert b.capacity()==100
    assert b.capacity() != 1000
    

def test_has_room_for():
    b=Box()
    t=Chose(100000000,"Le golem")
    assert b.has_room_for(t)
    b.set_capacity(1000)
    assert not b.has_room_for(t)




